Momentum is a CSS helper file that is lightweight and allows for rapid 
development. Momentum goes beyond a CSS reset by defining utility 
classes, but is most useful when using the Sass and Compass files.

### \_base.scss
Defines variables for colors and default fonts.

### \_breakpoint.scss
A placeholder for mobile and tablet styles with predefined breakpoints.

### \_grid.scss
Defines basic grid dimensions.

### \_helper.scss
Defines all helper functions and mixins. This includes basic grid layout functions, an em unit
utility, and functions a mixin for analyzing color contrast for background colors and foreground
text.

### \_print.scss
Basic print styles.

### \_reset.scss
A sass version of [http://git.io/normalize](http://git.io/normalize)

### \_utility.scss
Contains basic definition of utility classes for floats, clears, and content regions.
Uses the `%` operator so classes can be extended.

### \_styles.scss
This is the project specific file that glues the rest of the components together.

If you are using the Sass files, make sure you are running `compass watch`
from the directory that contains your `config.rb` file. 

## Releases
### v.3.0
Released Feb 19, 2014

### v.2.0
Released Sep 12, 2011

### v.1.0
Released Nov 19, 2008
